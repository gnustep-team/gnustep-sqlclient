Source: gnustep-sqlclient
Section: libs
Priority: optional
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Yavor Doganov <yavor@gnu.org>,
Build-Depends:
 debhelper-compat (= 13),
 default-libmysqlclient-dev,
 gnustep-base-doc <!nodoc>,
 libecpg-dev,
 libgnustep-base-dev (>= 1.30.0-10),
 libperformance-dev (>= 0.6.0-4),
 libsqlite3-dev,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: http://gnustep.org
Vcs-Git: https://salsa.debian.org/gnustep-team/gnustep-sqlclient.git
Vcs-Browser: https://salsa.debian.org/gnustep-team/gnustep-sqlclient

Package: libsqlclient-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libgnustep-base-dev,
 libsqlclient1.9 (= ${binary:Version}),
 ${misc:Depends},
Description: SQL client library for GNUstep (development files)
 The SQLClient library is designed to provide a simple interface to
 SQL databases for GNUstep applications.  It does not attempt the sort
 of abstraction provided by the much more sophisticated GDL2 library
 but rather allows applications to directly execute SQL queries and
 statements.
 Major features:
 .
  * Simple API for executing queries and statements.
  * Simple API for combining multiple SQL statements into a single
    transaction.
  * Supports multiple sumultaneous named connections to a database
    server in a thread-safe manner.
  * Supports multiple simultaneous connections to different database
    servers with backend driver bundles loaded for different database
    engines.
  * Configuration for all connections held in one place and referenced
    by connection name for ease of configuration control.
  * Thread safe operation.
 .
 Supported backend bundles are ECPG, Postgres, MySQL and SQLite.
 .
 This package contains the development files and the library reference
 documentation.

Package: libsqlclient1.9
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: SQL client library for GNUstep (runtime library)
 The SQLClient library is designed to provide a simple interface to
 SQL databases for GNUstep applications.  It does not attempt the sort
 of abstraction provided by the much more sophisticated GDL2 library
 but rather allows applications to directly execute SQL queries and
 statements.
 Major features:
 .
  * Simple API for executing queries and statements.
  * Simple API for combining multiple SQL statements into a single
    transaction.
  * Supports multiple sumultaneous named connections to a database
    server in a thread-safe manner.
  * Supports multiple simultaneous connections to different database
    servers with backend driver bundles loaded for different database
    engines.
  * Configuration for all connections held in one place and referenced
    by connection name for ease of configuration control.
  * Thread safe operation.
 .
 Supported backend bundles are ECPG, Postgres, MySQL and SQLite.
 .
 This package contains the shared library and the supported bundles.
